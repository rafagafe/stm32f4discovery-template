#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-eabi-gcc
CCC=arm-none-eabi-g++
CXX=arm-none-eabi-g++
FC=gfortran
AS=arm-none-eabi-as

# Macros
CND_PLATFORM=arm-none-eabi-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/1360937237/main.o \
	${OBJECTDIR}/_ext/2116868995/startup_stm32f407xx.o \
	${OBJECTDIR}/_ext/1360937237/stm32f4xx_hal_msp.o \
	${OBJECTDIR}/_ext/1360937237/stm32f4xx_it.o \
	${OBJECTDIR}/_ext/1360937237/system_stm32f4xx.o


# C Compiler Flags
CFLAGS=-T../src/startup/stm32_flash.ld -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16

# CC Compiler Flags
CCFLAGS=-T../src/startup/stm32_flash.ld -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16
CXXFLAGS=-T../src/startup/stm32_flash.ld -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../libcube/dist/Release/libcube.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/template.elf

${CND_DISTDIR}/${CND_CONF}/template.elf: ../libcube/dist/Release/libcube.a

${CND_DISTDIR}/${CND_CONF}/template.elf: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/template.elf ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.c) -O3 -DSTM32F407xx -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c

${OBJECTDIR}/_ext/2116868995/startup_stm32f407xx.o: ../src/startup/startup_stm32f407xx.s 
	${MKDIR} -p ${OBJECTDIR}/_ext/2116868995
	$(AS) $(ASFLAGS) -o ${OBJECTDIR}/_ext/2116868995/startup_stm32f407xx.o ../src/startup/startup_stm32f407xx.s

${OBJECTDIR}/_ext/1360937237/stm32f4xx_hal_msp.o: ../src/stm32f4xx_hal_msp.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.c) -O3 -DSTM32F407xx -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/stm32f4xx_hal_msp.o ../src/stm32f4xx_hal_msp.c

${OBJECTDIR}/_ext/1360937237/stm32f4xx_it.o: ../src/stm32f4xx_it.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.c) -O3 -DSTM32F407xx -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/stm32f4xx_it.o ../src/stm32f4xx_it.c

${OBJECTDIR}/_ext/1360937237/system_stm32f4xx.o: ../src/system_stm32f4xx.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.c) -O3 -DSTM32F407xx -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/system_stm32f4xx.o ../src/system_stm32f4xx.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/template.elf

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
