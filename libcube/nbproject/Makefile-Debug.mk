#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-eabi-gcc
CCC=arm-none-eabi-g++
CXX=arm-none-eabi-g++
FC=gfortran
AS=arm-none-eabi-as

# Macros
CND_PLATFORM=arm-none-eabi-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/216985314/stm32f4_discovery.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_adc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_adc_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_can.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cec.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cortex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_crc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cryp.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cryp_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dac.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dac_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dcmi.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dcmi_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma2d.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dsi.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_eth.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash_ramfunc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_fmpi2c.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_fmpi2c_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_gpio.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hash.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hash_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hcd.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2c.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2c_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2s.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2s_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_irda.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_iwdg.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_lptim.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_ltdc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_ltdc_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_nand.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_nor.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pccard.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pcd.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pcd_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pwr.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pwr_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_qspi.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rcc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rcc_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rng.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rtc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rtc_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sai.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sai_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sd.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sdram.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_smartcard.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_spdifrx.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_spi.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sram.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_tim.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_tim_ex.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_uart.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_usart.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_wwdg.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_fmc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_fsmc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_sdmmc.o \
	${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_usb.o \
	${OBJECTDIR}/_ext/1360937237/weak-objs.o


# C Compiler Flags
CFLAGS=-T../src/startup/stm32_flash.ld -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16

# CC Compiler Flags
CCFLAGS=-T../src/startup/stm32_flash.ld -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16
CXXFLAGS=-T../src/startup/stm32_flash.ld -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork -mfloat-abi=hard -mfpu=fpv4-sp-d16

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/libcube.a

${CND_DISTDIR}/${CND_CONF}/libcube.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/libcube.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/libcube.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/libcube.a

${OBJECTDIR}/_ext/216985314/stm32f4_discovery.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery/stm32f4_discovery.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/216985314
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/216985314/stm32f4_discovery.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery/stm32f4_discovery.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_adc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_adc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_adc_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_adc_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_can.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_can.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cec.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cec.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cec.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cec.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cortex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cortex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_crc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_crc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cryp.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cryp.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cryp_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_cryp_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dac.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dac.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dac_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dac_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dcmi.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dcmi.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dcmi_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dcmi_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma2d.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma2d.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dma_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dsi.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dsi.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_dsi.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dsi.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_eth.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_eth.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash_ramfunc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_flash_ramfunc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_fmpi2c.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_fmpi2c.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_fmpi2c_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_fmpi2c_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_fmpi2c_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_gpio.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_gpio.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hash.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hash.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hash_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hash_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hcd.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_hcd.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2c.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2c.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2c_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2c_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2s.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2s.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2s_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_i2s_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_irda.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_irda.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_iwdg.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_iwdg.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_lptim.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_lptim.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_lptim.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_lptim.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_ltdc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_ltdc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_ltdc_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_ltdc_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_nand.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_nand.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_nor.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_nor.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pccard.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pccard.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pcd.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pcd.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pcd_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pcd_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pwr.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pwr.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pwr_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_pwr_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_qspi.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_qspi.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_qspi.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_qspi.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rcc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rcc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rcc_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rcc_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rng.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rng.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rtc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rtc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rtc_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_rtc_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sai.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sai.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sai_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sai_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sd.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sd.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sdram.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sdram.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_smartcard.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_smartcard.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_spdifrx.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spdifrx.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_spdifrx.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spdifrx.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_spi.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_spi.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sram.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_sram.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_tim.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_tim.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_tim_ex.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_tim_ex.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_uart.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_uart.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_usart.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_usart.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_wwdg.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_hal_wwdg.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_fmc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_fmc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_fsmc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_fsmc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_sdmmc.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_sdmmc.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c

${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_usb.o: ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1241424071
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1241424071/stm32f4xx_ll_usb.o ../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c

${OBJECTDIR}/_ext/1360937237/weak-objs.o: ../src/weak-objs.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/1360937237
	${RM} "$@.d"
	$(COMPILE.c) -g -DSTM32F407xx -DUSE_FULL_ASSERT -I../src -I../../STM32Cube_FW_F4_V1.8.0/Drivers/STM32F4xx_HAL_Driver/Inc -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/CMSIS/Include -I../../STM32Cube_FW_F4_V1.8.0/Drivers/BSP/STM32F4-Discovery -std=c99 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1360937237/weak-objs.o ../src/weak-objs.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/libcube.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
