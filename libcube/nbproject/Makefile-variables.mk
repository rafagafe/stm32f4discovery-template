#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=arm-none-eabi-Linux-x86
CND_ARTIFACT_DIR_Debug=dist/Debug
CND_ARTIFACT_NAME_Debug=libcube.a
CND_ARTIFACT_PATH_Debug=dist/Debug/libcube.a
CND_PACKAGE_DIR_Debug=dist/Debug/package
CND_PACKAGE_NAME_Debug=libcube.tar
CND_PACKAGE_PATH_Debug=dist/Debug/package/libcube.tar
# Release configuration
CND_PLATFORM_Release=arm-none-eabi-Linux-x86
CND_ARTIFACT_DIR_Release=dist/Release
CND_ARTIFACT_NAME_Release=libcube.a
CND_ARTIFACT_PATH_Release=dist/Release/libcube.a
CND_PACKAGE_DIR_Release=dist/Release/package
CND_PACKAGE_NAME_Release=libcube.tar
CND_PACKAGE_PATH_Release=dist/Release/package/libcube.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
